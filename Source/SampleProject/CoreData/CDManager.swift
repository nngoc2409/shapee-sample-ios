//
//  CDManager.swift
//  SampleProject
//
//  Created by apple on 11/20/20.
//

import UIKit
import JustPersist
import CoreData

enum CDStack: String {
    case SampleProject
}

class CDManager: NSObject {
    static let me = CDManager()
    var store: DataStore!
    var child: ChildDataStore!
    
    var countries: [CDCountry] = []
    
    override init() {
        store = MagicalRecordDataStore.stack(CDStack.SampleProject.rawValue as NSString, securityApplicationGroupIdentifier: Api.me.app.rawValue as NSString, errorHandler: { (error) in
            Log.me.error("Config core data store fail: \(error)")
        })
        store.setup()
        child = store.makeChildDataStore()
        child.setup()
    }
}
//MARK: -- Country --
extension CDManager {
    func getAllCountry() {
        store.read { (acc) in
            let request = DataStoreRequest(itemType: CDCountry.self)
            self.countries = acc.items(forRequest: request)
        }
    }
    
    func getCountryWith(name: String) {
        store.read { (acc) in
            let request = DataStoreRequest(itemType: CDCountry.self)
            request.setFilter(whereAttribute: "name", equalsValue: name)
            self.countries = acc.items(forRequest: request)
        }
    }
    
    func saveCountriesWith(cts: [Country], _ block: @escaping () -> Void) {
        let gr = GCDGroupOperations()
        for ct in cts {
            gr.enter {
                ct.writeAsync {
                    gr.leave()
                }
            }
        }
        gr.notify {
            self.store.merge(self.child)
            block()
        }
    }
}
