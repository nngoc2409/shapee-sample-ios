//
//  Currency.swift
//  TemplateProject
//
//  Created by Benoit PASQUIER on 13/01/2018.
//  Copyright © 2018 Benoit PASQUIER. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: - Country
struct Country: Codable {
    var name: String = ""
    var region: String = ""
    var latlng: [Int] = []
    var currencies: [String] = []
    var languages: [String] = []
    var translations: Translations = Translations()
    
    init() {
    }
    
    init(json: JSON) {
        name = json["name"].stringValue
        region = json["region"].stringValue
        latlng = []
        for js in json["latlng"].arrayValue {
            latlng.append(js.intValue)
        }
        currencies = []
        for js in json["currencies"].arrayValue {
            currencies.append(js.stringValue)
        }
        languages = []
        for js in json["languages"].arrayValue {
            languages.append(js.stringValue)
        }
        translations = Translations(json: json["translations"])
    }
}

//MARK: -- Core Data --
extension Country {
    func writeSync() {
        CDManager.me.store.writeSync { (acc) in
            let model = acc.createItem(ofMutableType: CDCountry.self) as! CDCountry
            model.name = name
            model.region = region
            model.latlng = latlng
            model.currencies = currencies
            model.languages = languages
            let tr = acc.createItem(ofMutableType: CDTranslation.self) as! CDTranslation
            translations.toCDModel(model: tr)
            model.translations = tr
        }
    }
    
    func writeAsync(_ block: @escaping () -> Void) {
        CDManager.me.child.writeAsync { (acc) in
            let model = acc.createItem(ofMutableType: CDCountry.self) as! CDCountry
            model.name = name
            model.region = region
            model.latlng = latlng
            model.currencies = currencies
            model.languages = languages
            let tr = acc.createItem(ofMutableType: CDTranslation.self) as! CDTranslation
            translations.toCDModel(model: tr)
            model.translations = tr
            block()
        }
    }
}

//MARK: -- Realm --
extension Country {
    func toRealm() -> RCountry {
        let model = RCountry()
        model.name = name
        model.region = region
        for ob in latlng {
            model.latlng.append(ob)
        }
        for ob in languages {
            model.languages.append(ob)
        }
        for ob in currencies {
            model.currencies.append(ob)
        }
        model.translations = translations.toRealm()
        return model
    }
}

// MARK: - Translations
struct Translations: Codable {
    var de: String = ""
    var es: String = ""
    var fr: String = ""
    var ja: String = ""
    var it: String = ""
    
    init() {
    }
    
    init(json: JSON) {
        de = json["de"].stringValue
        es = json["es"].stringValue
        fr = json["fr"].stringValue
        ja = json["ja"].stringValue
        it = json["it"].stringValue
    }
}

//MARK: -- Core Data --
extension Translations {
    func toCDModel(model: CDTranslation) {
        model.de = de
        model.es = es
        model.fr = fr
        model.ja = ja
        model.it = it
    }
}

extension Translations {
    func toRealm() -> RTranslation {
        let model = RTranslation()
        model.de = de
        model.es = es
        model.fr = fr
        model.ja = ja
        model.it = it
        return model
    }
}
