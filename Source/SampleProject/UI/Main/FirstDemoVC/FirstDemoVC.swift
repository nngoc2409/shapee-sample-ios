//
//  FirstDemoVC.swift
//  SampleProject
//
//  Created by apple on 11/6/20.
//

import UIKit
import RealmSwift
import Permission

class FirstDemoVC: BaseVC {
    var results: Results<RCountry>? = nil
    var notificationToken: NotificationToken? = nil
    @IBOutlet weak var tbMain: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        testGCDGroup()
//        reloadCountry()
        initUI()
        permisson()
    }
}

extension FirstDemoVC {
    func testGCDGroup() {
        let group = GCDGroupOperations()
        group.enter {
            Log.me.info("Group gcd 1")
            CountryManager.me.syncCountrys(progress: true) { (cts) in
                Log.me.info("Sync country")
                group.leave()
            }
        }
        group.enter {
            Log.me.warning("Group gcd 2")
            group.leave()
        }
        group.notify {
            Log.me.error("End group gcd")
        }
    }
    
    func reloadCountry() {
        results = Database.me.database?.objects(RCountry.self)
        notificationToken = results?.observe({ (change) in
            Log.me.debug(change)
            switch change {
            case .initial:
                break
            case .update(_, let deletions, let insertions, let modifications):
                Log.me.info("delete: \(deletions), insert: \(insertions), modifier: \(modifications)")
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                Log.me.error(error)
            }
        })
    }
    
    func saveRealm(cts: [Country]) {
        Database.me.clean()
        var arr: [RCountry] = []
        for ct in cts {
            arr.append(ct.toRealm())
        }
        Database.me.save(objects: arr)
    }
    
    func permisson() {
        let permission: Permission = .photos
        Log.me.debug(permission.status)
        permission.request { (status) in
            switch status {
            case .authorized:    print("authorized")
            case .denied:        print("denied")
            case .disabled:      print("disabled")
            case .notDetermined: print("not determined")
            }
        }
    }
    
    func initUI() {
        isRefresh = true
        isLoadmore = true
        scrollView = tbMain
    }
}
