//
//  BaseVC.swift
//  SampleProject
//
//  Created by apple on 11/6/20.
//

import UIKit

class BaseVC: UIViewController {
    typealias BaseSyncViewControllerSyncCompleteCallback = (Bool, [AnyHashable]?, Error?) -> Void
    typealias BaseSyncViewControllerSyncCallback = ([AnyHashable : Any]?, Int, Int, Bool, Bool, BaseSyncViewControllerSyncCompleteCallback) -> Void
    
    var syncCallback: BaseSyncViewControllerSyncCallback? = nil
    var scrollView: UIScrollView? {
        didSet {
            self.setupControl()
        }
    }
    var isRefresh: Bool = false
    var isLoadmore: Bool = false
    var shouldSyncOnInit: Bool = false
    var fetchedOnce: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension BaseVC {
    func setupControl() {
        guard let sr = scrollView else {
            return
        }
        if isRefresh {
        }
        if isLoadmore {
        }
    }
    
    func refresh() {
        
    }
    
    func loadmore() {
    }
    
    func endControl() {
    }
    
    func addBackNav() {
    }
}
