//
//  AppController.swift
//  SampleProject
//
//  Created by apple on 11/6/20.
//

import UIKit
import SwiftyBeaver

class AppController: NSObject {
    static let me = AppController()
    let del = UIApplication.shared.delegate as! AppDelegate
}

extension AppController {
    func confige() {
        NetworkServices.me.configure()
        let configuration = DatabaseConfiguration(name: "", type: .disk, debug: .error)
        Database.me.configure(configuration: configuration)
        PushManager.me.register()
        setupLog()
        setupRootVC()
    }
    
    func setupRootVC() {
        del.window = UIWindow(frame: UIScreen.main.bounds)
        del.window?.rootViewController = FirstDemoVC().rootNav()
        del.window?.makeKeyAndVisible()
    }
    
    func setupLog() {
        let console = ConsoleDestination()
        let file = FileDestination()
        SwiftyBeaver.addDestination(console)
        SwiftyBeaver.addDestination(file)
    }
}
