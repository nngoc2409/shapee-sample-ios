//
//  AppConstants.swift
//  SampleProject
//
//  Created by apple on 11/6/20.
//

import Foundation

enum SaveKey: String {
    case isFirstLaunch = "isFirstLaunch"
}

enum KeychainKey: String {
    case isFirstLaunch = "isFirstLaunch"
}

enum BundleIdentifier: String {
    case dev = "com.shapee.sampleproject.dev"
    case live = "com.shapee.sampleproject.live"
}

class Constants {
    static let serverTimeout = 30
    static let X_RapidAPI_Host = "restcountries-v1.p.rapidapi.com"
    static let X_RapidAPI_Key = "10de00e0afmshd1225b302b8135ap199d3cjsnec68e33b481c"
}

class RealmConstants {
    static func realmSchemaVersion() -> UInt64 {
        //20-11-2020: 1
        //20-11-2020: 2 try change
        //20-11-2020: 3 try change
        return 3
    }
    
    static func realmDbKey() -> String {
        return "Any_Key"
    }
}
