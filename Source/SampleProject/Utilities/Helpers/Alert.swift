////
////  AlertHelper.swift
////  Task
////
////  Created by Hoa on 3/31/18.
////  Copyright © 2018 SDC. All rights reserved.
////
//
//import UIKit
//
//public typealias ActionHandler = (_ action: UIAlertAction) -> ()
//public typealias AttributedActionTitle = (title: String, style: UIAlertAction.Style)
//
//class AlertController: UIAlertController {
//    @objc
//    func hideAlertController() {
//        self.dismiss(animated: true, completion: nil)
//    }
//}
//
//class Alert {
//    static func show(_ viewController: UIViewController,_ strMessage: String){
//        self.dismissAllAlert()
//        let alert = AlertController(title: "app_name".localized, message: strMessage.localized, preferredStyle: UIAlertController.Style.alert)
//        let okAction: UIAlertAction = UIAlertAction(title: "txt_ok".localized, style: .cancel) { action -> Void in
//            
//        }
//        NotificationManager.add(observer: alert, selector: #selector(AlertController.hideAlertController), name: .DismissAllAlert)
//        alert.addAction(okAction)
//        viewController.present(alert, animated: true, completion: nil)
//    }
//    
//    static func show(_ strMessage: String){
//        self.dismissAllAlert()
//        _ = UIAlertController.present(style: .alert, title: "app_name".localized, message: strMessage.localized, attributedActionTitles: [("txt_ok".localized, .default)], handler: { (action) in
//            if action.title == "OK" {
//                
//            }
//        })
//    }
//    
//    static func showWithOneAction(_ strMessage: String, strTitle: String = "OK", handle: @escaping (() -> ())){
//        self.dismissAllAlert()
//        _ = UIAlertController.present(style: .alert, title: "app_name".localized, message: strMessage.localized, attributedActionTitles: [(strTitle, .default)], handler: { (action) in
//            if action.title == strTitle {
//                handle()
//            }
//        })
//    }
//    
//    static func show(_ viewController: UIViewController,_ strTitle: String,_ strMessage: String){
//        self.dismissAllAlert()
//        let alert = AlertController(title: strTitle, message: strMessage.localized, preferredStyle: UIAlertController.Style.alert)
//        let okAction: UIAlertAction = UIAlertAction(title: "txt_ok".localized, style: .cancel) { action -> Void in
//            
//        }
//        NotificationManager.add(observer: alert, selector: #selector(AlertController.hideAlertController), name: .DismissAllAlert)
//        alert.addAction(okAction)
//        viewController.present(alert, animated: true, completion: nil)
//    }
//    
//    static func dismissAllAlert() {
//        NotificationManager.post(name: .DismissAllAlert)
//    }
//}
//
//public extension UIAlertController {
//    
//    class func present(style: UIAlertController.Style = .alert, title: String?, message: String?, actionTitles: [String]?, handler: ActionHandler? = nil) -> UIAlertController {
//        let rootViewController = UIApplication.shared.delegate!.window!!.rootViewController!
//        return self.presentFromViewController(viewController: rootViewController, style: style, title: title, message: message, actionTitles: actionTitles, handler: handler)
//    }
//    
//    class func present(style: UIAlertController.Style = .alert, title: String?, message: String?, attributedActionTitles: [AttributedActionTitle]?, handler: ActionHandler? = nil) -> UIAlertController {
//        let rootViewController = UIApplication.shared.delegate!.window!!.rootViewController!
//        return self.presentFromViewController(viewController: rootViewController, style: style, title: title, message: message, attributedActionTitles: attributedActionTitles, handler: handler)
//    }
//    
//    class func presentFromViewController(viewController: UIViewController, style: UIAlertController.Style = .alert, title: String?, message: String?, actionTitles: [String]?, handler: ActionHandler? = nil) -> UIAlertController {
//        return self.presentFromViewController(viewController: viewController, style: style, title: title, message: message, attributedActionTitles: actionTitles?.map({ (title) -> AttributedActionTitle in return (title: title, style: .default) }), handler: handler)
//    }
//    
//    class func presentFromViewController(viewController: UIViewController, style: UIAlertController.Style = .alert, title: String?, message: String?, attributedActionTitles: [AttributedActionTitle]?, handler: ActionHandler? = nil) -> UIAlertController {
//        let alertController = AlertController(title: title?.localized, message: message?.localized, preferredStyle: style)
//        if let _attributedActionTitles = attributedActionTitles {
//            for _attributedActionTitle in _attributedActionTitles {
//                let buttonAction = UIAlertAction(title: _attributedActionTitle.title, style: _attributedActionTitle.style, handler: { (action) -> Void in
//                    handler?(action)
//                })
//                alertController.addAction(buttonAction)
//            }
//        }
//        NotificationManager.add(observer: alertController, selector: #selector(AlertController.hideAlertController), name: .DismissAllAlert)
//        alertController.popoverPresentationController?.sourceView = viewController.view
//        alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
//        alertController.popoverPresentationController?.sourceRect = CGRect(x: viewController.view.bounds.midX, y: viewController.view.bounds.midY, width: 0, height: 0)
//        viewController.present(alertController, animated: true) {
//            print("option menu presented")
//        }
//        return alertController
//    }
//}
//
//// MARK:
//public extension UIViewController {
//    func presentAlert(style: UIAlertController.Style = .alert, title: String?, message: String?, actionTitles: [String]?, handler: ActionHandler? = nil) -> UIAlertController {
//        return UIAlertController.presentFromViewController(viewController: self, style: style, title: title, message: message, actionTitles: actionTitles, handler: handler)
//    }
//    
//    func presentAlert(style: UIAlertController.Style = .alert, title: String?, message: String?, attributedActionTitles: [AttributedActionTitle]?, handler: ActionHandler? = nil) -> UIAlertController {
//        return UIAlertController.presentFromViewController(viewController: self, style: style, title: title, message: message, attributedActionTitles: attributedActionTitles, handler: handler)
//    }
//    
//    var topPresentedViewController: UIViewController? {
//        get {
//            var target: UIViewController? = self
//            while (target?.presentedViewController != nil) {
//                target = target?.presentedViewController
//            }
//            return target
//        }
//    }
//    
//    var topVisibleViewController: UIViewController? {
//        get {
//            if let nav = self as? UINavigationController {
//                return nav.topViewController?.topVisibleViewController
//            }
//            else if let tabBar = self as? UITabBarController {
//                return tabBar.selectedViewController?.topVisibleViewController
//            }
//            return self
//        }
//    }
//    
//    var topMostViewController: UIViewController? {
//        get {
//            return self.topPresentedViewController?.topVisibleViewController
//        }
//    }
//    
//}
//
//extension UIViewController {
//    var className: String {
//        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
//    }
//}
