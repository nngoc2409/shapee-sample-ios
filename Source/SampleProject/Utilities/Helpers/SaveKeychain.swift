//
//  SaveKeychain.swift
//  SampleProject
//
//  Created by apple on 11/6/20.
//

import Foundation
import KeychainAccess

class SaveKeychain {
    static let keychain = Keychain(service: "com.shapee.sampleproject")
    
    static func save(_ object: String, key: KeychainKey) {
        do {
            try keychain.set(object, key: key.rawValue)
        } catch {
        }
    }
    
    static func get(_ key: KeychainKey) -> String {
        do {
            let value = try keychain.get(key.rawValue)
            return value ?? ""
        } catch {
            return ""
        }
    }
}
