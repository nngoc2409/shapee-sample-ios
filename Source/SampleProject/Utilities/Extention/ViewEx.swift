//
//  ViewExtention.swift
//  CashD
//
//  Created by Phạm Hoà on 7/16/19.
//  Copyright © 2019 Luy Nguyen. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @IBInspectable var kBorderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var kBorderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var kCornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var kCircle: Bool {
        set {
            DispatchQueue.main.async {
                self.layer.cornerRadius = self.bounds.height/2
                self.layer.masksToBounds = true
            }
        }
        get {
            return layer.masksToBounds
        }
    }
    
    @IBInspectable var kShadowColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.shadowColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var kShadowOffset: CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }
    
    @IBInspectable var kShadowOpacity: Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    
    @IBInspectable var kShadowRadius: CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
    
    @IBInspectable var kShadowMasksToBounds: Bool {
        set {
            layer.masksToBounds = newValue
        }
        get {
            return layer.masksToBounds
        }
    }
}

extension UIViewController {
    func rootNav() -> UINavigationController {
        return UINavigationController(rootViewController: self)
    }
}
