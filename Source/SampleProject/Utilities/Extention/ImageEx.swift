//
//  ImageExtention.swift
//  TemplateProject
//
//  Created by Hoa on 7/1/19.
//  Copyright © 2019 Benoit PASQUIER. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func imageWithUrl(_ urlString: String, placeholder: UIImage) {
        if urlString.count > 0 {
            self.kf.setImage(with: URL.init(string: urlString), placeholder: placeholder, options: [.transition(ImageTransition.fade(1))], progressBlock: { (receivedSize, totalSize) in
                
            }) { (result) in
                switch result {
                case .success(let value):
                    if value.source.url?.absoluteString == urlString {
                        GCDCommon.mainQueue {
                            self.image = value.image
                        }
                    }
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        }
    }
    
    func imageWithUrl(_ urlString: String) {
        if urlString.count > 0 {
            self.kf.setImage(with: URL.init(string: urlString), placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: { (receivedSize, totalSize) in
                
            }) { (result) in
                switch result {
                case .success(let value):
                    if value.source.url?.absoluteString == urlString {
                        self.image = value.image
                    }
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        }
    }
}
