//
//  Database.swift
//  Boostcode
//
//  Created by Matteo Crippa on 3/20/17.
//  Copyright © 2017 Boostco.de. All rights reserved.
//

import Foundation
import RealmSwift

public enum DatabaseDebugVerbosity {
    case none
    case all
    case error
    case message
}

public enum DatabaseWriteType: String {
    case memory = "inMemory"
    case disk = "onDisk"
}

/// Database configuration struct
open class DatabaseConfiguration{
    var name = ""
    var type: DatabaseWriteType = .disk
    var debug: DatabaseDebugVerbosity = .none
    
    public init(name: String = "", type: DatabaseWriteType = .disk, debug: DatabaseDebugVerbosity = .none) {
        self.name = name
        self.type = type
        self.debug = debug
    }
}

fileprivate protocol Databaseable: class {
    static var me: Database { get }
    
    var database: Realm? { get }
    var configuration: DatabaseConfiguration? { get }
    
    func configure(configuration: DatabaseConfiguration)
    
    func get<T: Object>(type: T.Type) -> Results<T>?
    func get<T: Object, K>(type: T.Type, key: K) -> T?
    
    func save(object: Object)
    func save<S: Sequence>(objects: S) where S.Iterator.Element: Object
    
    func update(object: Object)
    func update<S: Sequence>(objects: S) where S.Iterator.Element: Object
    
    func clean()
}

fileprivate extension Databaseable {
    func connectDb(name: String, type: DatabaseWriteType = .disk, debug: Bool = false) {}
}

open class Database: Databaseable {
    /// Shared instance
    public static var me = Database()
    private init() {}
    
    /// Database handler
    open var database: Realm?
    
    /// Database configuration
    var configuration: DatabaseConfiguration?
    
    open func configure(configuration: DatabaseConfiguration) {
        
        // set debug status
        self.configuration = configuration
        
        // create conf file
        var dbConf = Realm.Configuration()
        
        switch configuration.type {
        case .memory:
            dbConf.fileURL = nil
            dbConf.inMemoryIdentifier = configuration.type.rawValue
        case .disk:
            break
        }
        
        dbConf.encryptionKey = Data(base64Encoded: RealmConstants.realmDbKey(), options: [])
        // we want full RW
        dbConf.readOnly = false
        
        // force schema to 0
        dbConf.schemaVersion = RealmConstants.realmSchemaVersion()
        
        dbConf.shouldCompactOnLaunch = { totalBytes, usedBytes in
            let oneHundredMB = 100 * 1024 * 1024
            return (totalBytes > oneHundredMB) && Double((usedBytes / totalBytes)) < 0.5
        }
        let path = URL(fileURLWithPath: URL(fileURLWithPath: dbConf.fileURL?.path ?? "").deletingLastPathComponent().path).appendingPathComponent("demo.realm").path
        dbConf.fileURL = URL(fileURLWithPath: path)
        
        // setup database
        do {
            database = try Realm(configuration: dbConf)
        } catch (let e) {
            debug(error: e.localizedDescription)
        }
        
        // show info for database if verbose
        settings(data: "🔌 Database Setup")
        settings(data: dbConf.description)
        settings(data: "🗺 Path to realm file: " + (database?.configuration.fileURL!.absoluteString)!)
    }
    
    /// Get an object of type for key
    ///
    /// - Parameters:
    ///   - type: object type
    ///   - key: key to filter items
    /// - Returns: return an object
    open func get<T: Object, K>(type: T.Type, key: K) -> T? {
        
        guard let database = database else {
            debug(error: "instance not available")
            return nil
        }
        
        return database.object(ofType: type, forPrimaryKey: key)
    }
    
    /// Get all object of type
    ///
    /// - Parameter type: type of object
    /// - Returns: return a result list of type
    open func get<T: Object>(type: T.Type) -> Results<T>? {
        
        guard let database = database else {
            debug(error: "instance not available")
            return nil
        }
        
        return database.objects(type)
        
    }
    
    /// Save a sequence of object in database
    ///
    /// - Parameter objects: objects to be saved
    open func save<S: Sequence>(objects: S) where S.Iterator.Element: Object {
        /// Get the current database instance
        guard let database = database else {
            debug(error: "instance not available")
            return
        }
        
        do {
            try database.write {
                debug(data: Array(objects).description)
                database.add(objects)
            }
        } catch(let e) {
            debug(error: e.localizedDescription)
        }
    }
    
    /// Save an object in database
    ///
    /// - Parameter object: object item
    open func save(object: Object) {
        
        /// Get the current database instance
        guard let database = database else {
            debug(error: "instance not available")
            return
        }
        
        do {
            try database.write {
                debug(data: object.description)
                database.add(object)
            }
        } catch(let e) {
            debug(error: e.localizedDescription)
        }
    }
    
    /// Save a sequence of object in database
    ///
    /// - Parameter objects: objects to be saved
    open func update<S: Sequence>(objects: S) where S.Iterator.Element: Object {
        /// Get the current database instance
        guard let database = database else {
            debug(error: "instance not available")
            return
        }
        
        do {
            try database.write {
                debug(data: Array(objects).description)
                database.add(objects, update: .modified)
            }
        } catch(let e) {
            debug(error: e.localizedDescription)
        }
    }
    
    /// Save an object in database
    ///
    /// - Parameter object: object item
    open func update(object: Object) {
        
        /// Get the current database instance
        guard let database = database else {
            debug(error: "instance not available")
            return
        }
        
        do {
            try database.write {
                debug(data: object.description)
                database.add(object, update: .modified)
            }
        } catch(let e) {
            debug(error: e.localizedDescription)
        }
    }
    
    /// Delete objects from realm
    ///
    /// - Parameter object: Object of realm type
    open func delete(object: Object) {
        
        /// Get the current database instance
        guard let database = database else {
            debug(error: "instance not available")
            return
        }
        
        do {
            try database.write {
                debug(data: object.description)
                database.delete(object)
            }
        } catch(let e) {
            debug(error: e.localizedDescription)
        }
    }
    
    /// Delete objects from realm
    ///
    /// - Parameter object: Object of realm type
    open func clean() {
        
        /// Get the current database instance
        guard let database = database else {
            debug(error: "instance not available")
            return
        }
        
        do {
            try database.write {
                debug(data: "remove all")
                database.deleteAll()
            }
        } catch(let e) {
            debug(error: e.localizedDescription)
        }
    }
    
    /// Error debug log wrapper for db
    ///
    /// - Parameter error: string error
    fileprivate func debug(error: String) {
        if let configuration = configuration {
            if configuration.debug == .all || configuration.debug == .error {
                print("🗄❌ Database Error ❌ > " + error)
            }
        }
    }
    
    /// Action debug log wrapper for db
    ///
    /// - Parameter data: string
    fileprivate func debug(data: String) {
        if let configuration = configuration {
            if configuration.debug == .all || configuration.debug == .message {
                print("🗄👉 Database > " + data)
            }
        }
    }
    
    /// Action settings log wrapper for db
    ///
    /// - Parameter data: string
    fileprivate func settings(data: String) {
        if let configuration = configuration {
            if configuration.debug == .all || configuration.debug == .message || configuration.debug == .error {
                print("🗄🎚 Database > " + data)
            }
        }
    }
    
}
