//
//  RealmModel.swift
//  SampleProject
//
//  Created by apple on 11/20/20.
//

import UIKit
import RealmSwift

class RCountry: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var region: String = ""
    var latlng: List<Int> = List()
    var currencies: List<String> = List()
    var languages: List<String> = List()
    @objc dynamic var translations: RTranslation?
}

class RTranslation: Object {
    @objc dynamic var de: String = ""
    @objc dynamic var es: String = ""
    @objc dynamic var fr: String = ""
    @objc dynamic var ja: String = ""
    @objc dynamic var it: String = ""
}
