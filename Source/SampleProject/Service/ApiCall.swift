//
// MGConnection.swift.
// NewAPIService.
// 

import Foundation
import KRProgressHUD
import SwiftyJSON
import Alamofire

class ApiCall: NSObject {
    static let me = ApiCall()
    
    func request(_ apiRouter: URLRequestConvertible, hasProgress: Bool, completion: @escaping (JsonResult<JSON, AFError>) -> Void) {
        if !NetworkServices.isNetworkAvailable() {
            return
        }
        if hasProgress {
            KRProgressHUD.show()
        }
        AF.request(apiRouter).responseJSON { (response) in
            if hasProgress {
                KRProgressHUD.dismiss()
            }
            switch response.result {
            case .success(let response):
                let json = JSON(response)
                print("- result: \(json)")
                completion(.success(json))
                break
            case .failure(let error):
                print(error.localizedDescription)
                completion(.failure(error))
                break
            }
        }
    }
}

enum JsonResult<T, AFError> {
    case success(T)
    case failure(AFError)
}
