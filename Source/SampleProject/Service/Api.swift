//
//  API.swift
//  HMKFieldCollector
//
//  Created by Luy Nguyen on 11/21/18.
//  Copyright © 2018 Hoa. All rights reserved.
//

import Foundation

class Api: NSObject {
    static let me = Api()
    var baseUrl: String = "https://restcountries-v1.p.rapidapi.com"
    var app: BundleIdentifier = .dev
    
    override init() {
        let bundleId = Bundle.main.bundleIdentifier ?? ""
        app = BundleIdentifier(rawValue: bundleId) ?? .dev
        switch app {
        case .dev:
            baseUrl = "https://restcountries-v1.p.rapidapi.com"
            break
        default:
            baseUrl = "https://restcountries-v1.p.rapidapi.com"
            break
        }
    }
}
