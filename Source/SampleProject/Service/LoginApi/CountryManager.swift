//
//  LoginManager.swift
//  CashD
//
//  Created by Shapee Hoa on 1/9/20.
//  Copyright © 2020 Luy Nguyen. All rights reserved.
//

import Foundation

class CountryManager: NSObject {
    static let me = CountryManager()
    
    func syncCountrys(progress: Bool, _ completion: @escaping (([Country]) -> Void)) {
        ApiCall.me.request(CountryRouter.all, hasProgress: progress) { (result) in
            switch result {
            case .success(let response):
                var cts: [Country] = []
                for json in response.arrayValue {
                    cts.append(Country(json: json))
                }
                completion(cts)
                break
            case .failure(_):
                completion([])
                break
            }
        }
    }
}
