//
//  CountryApi.swift
//  TemplateProject
//
//  Created by Hoa on 6/28/19.
//  Copyright © 2019 Benoit PASQUIER. All rights reserved.
//

import Foundation
import Alamofire

enum CountryRouter: URLRequestConvertible {
    case all
    case get(id: String)
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .all: return "/all"
        case .get(let id):
            return "/get/\(id)"
        }
    }
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .all: return .get
        default:
            return .get
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .all:
            return nil
        default:
            return nil
        }
    }
    
    // MARK: - headers
    private var headers: HTTPHeaders {
        switch self {
        case .all:
            return ["Content-Type": "application/json",
                    "X-RapidAPI-Host": Constants.X_RapidAPI_Host,
                    "X-RapidAPI-Key": Constants.X_RapidAPI_Key]
        default:
            return ["Content-Type": "application/json"]
        }
    }
    
    // MARK: - array params
    private var arrayParam: [[String: Any]]? {
        return nil
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        return try URLRequest.createRequest(with: path, method, arrayParam, parameters, headers)
    }
}
