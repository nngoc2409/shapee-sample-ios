import UIKit
import UserNotifications
import Permission

class PushManager: NSObject {
    static let me = PushManager()
    
    var deviceToken:Data?
    
    func register() {
        let per: Permission = .notifications
        per.request { (status) in
            if status != .authorized {
                self.setupCategories()
            }
        }
    }
    
    func register(deviceToken: Data) {
        self.deviceToken = deviceToken
        print("Device Token: \(deviceToken.token())")
    }
    
    func setupCategories() {
        let remember = UNNotificationAction(
          identifier: Identifiers.remember, title: "I remember this word (count -1)",
            options: [.foreground])
        let notSure = UNNotificationAction(
        identifier: Identifiers.notSure, title: "I’m not Sure (count +1)",
        options: [.foreground])
        let notWant = UNNotificationAction(
        identifier: Identifiers.notWant, title: "Don’t Want To See Again (count =0)",
        options: [.foreground])
        let newsCategory = UNNotificationCategory(
            identifier: Identifiers.category, actions: [remember, notSure, notWant],
          intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current()
          .setNotificationCategories([newsCategory])
    }
}

extension PushManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions.alert)
    }
}

extension Data {
    func token() -> String {
        let tokenParts = self.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        return tokenParts.joined()
    }
}

class Identifiers {
    static let remember = "remember"
    static let notSure = "notSure"
    static let notWant = "notWant"
    static let category = "test.push"
}
